# Deploy Web App on Heroku

Deploy Web App on Heroku

https://csfcv.herokuapp.com/

#Create a Procfile and add following into it as its content,

> web: gunicorn cv_web_app:app>

#create a requirements.txt file and add the following list of python packges,

> Flask
> gunicorn
> numpy
> opencv-contrib-python-headless


```
git init

git add .

git commit -m "Initial Commit"

sudo snap install --classic heroku

heroku login -i

heroku create csfcv

git push heroku master
```
